import { Container } from "@mui/material"
import ComponentCarousel from "./ComponentCarousel"
import LastestProducts from "./LastestProducts"
import ViewAll from "./ViewAll"



function Content (){
    return(
      <Container>
        <ComponentCarousel></ComponentCarousel>
        <LastestProducts></LastestProducts>
        <ViewAll></ViewAll>
      </Container>
    )
}

export default Content